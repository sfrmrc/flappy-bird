package it.rome.flappy.level;

import java.util.Collection;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Json;


/**
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public class Level {

  public Collection<Pipe> pipes;

  public Collection<Pipe> getPipes() {
    return pipes;
  }

  public void setPipes(Collection<Pipe> pipes) {
    this.pipes = pipes;
  }

  public static class Reader {

    private static final Json json = new Json();
    private static final Level level = new Level();

    public static Level load(String file) {
      try {
        return json.fromJson(level.getClass(), Gdx.files.internal(file));
      } catch (Exception e) {
        Gdx.app.log("Error", e.getMessage());
      }
      return level;
    }

  }

}
