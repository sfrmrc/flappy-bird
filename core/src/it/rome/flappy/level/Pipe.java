package it.rome.flappy.level;


/**
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public class Pipe {

  public static final float BASE = 300f;

  public float x;
  public float height;
  public String type;

  public Pipe() {}

  public float getHeight() {
    return height;
  }

  public void setHeight(float height) {
    this.height = height;
  }

  public float getX() {
    return x;
  }

  public void setX(float x) {
    this.x = x;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getType() {
    return type;
  }

}
