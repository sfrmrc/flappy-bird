package it.rome.flappy.system;

import it.rome.flappy.GameObjectFactory;
import it.rome.game.component.CollisionComponent;
import it.rome.game.system.CollisionsSystem;
import it.rome.game.system.ScriptSystem;
import it.rome.game.system.SpritePositionSystem;

import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.managers.TagManager;
import com.artemis.systems.VoidEntitySystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;

/**
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
@Wire
public class FlappyGameDirectorSystem extends VoidEntitySystem {

  ComponentMapper<CollisionComponent> cm;

  SpritePositionSystem spritePositionSystem;
  ScriptSystem scriptSystem;
  TapInputSystem tapInputSystem;
  CollisionsSystem collisionsSystem;

  TagManager tagManager;

  String playerTag;

  float w;
  float h;

  boolean dead = false;
  boolean gameOver = false;

  public FlappyGameDirectorSystem(float w, float h, String playerTag) {
    this.playerTag = playerTag;
    this.w = w;
    this.h = h;
  }

  @Override
  protected void processSystem() {
    Entity player = tagManager.getEntity(playerTag);
    if (dead) {
      if (Gdx.input.justTouched() || Gdx.input.isKeyJustPressed(Keys.SPACE)) {
        gameOver = true;
      }
    }
    if (cm.get(player).getContact().size() != 0) {
      spritePositionSystem.setEnabled(false);
      scriptSystem.setEnabled(false);
      tapInputSystem.setEnabled(false);
      collisionsSystem.setEnabled(false);
      dead = true;
      GameObjectFactory.createText("Game Over", w * .5f, h * .5f + 150);
    }
  }

  public boolean isGameOver() {
    return gameOver;
  }

}
