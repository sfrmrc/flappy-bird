package it.rome.flappy.system;

import it.rome.game.component.ScoreComponent;
import it.rome.game.component.TextComponent;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;

/**
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
@Wire
public class ScoreIndicatorSystem extends EntityProcessingSystem {

  ComponentMapper<ScoreComponent> sm;
  ComponentMapper<TextComponent> tm;

  @SuppressWarnings("unchecked")
  public ScoreIndicatorSystem() {
    super(Aspect.getAspectForAll(ScoreComponent.class, TextComponent.class));
  }

  @Override
  protected void process(Entity entity) {
    TextComponent textComponent = tm.get(entity);
    ScoreComponent scoreComponent = sm.get(entity);
    textComponent.setText(String.valueOf(scoreComponent.getScore()));
  }

}
