package it.rome.flappy.system;

import it.rome.flappy.Tags;
import it.rome.game.component.ScoreComponent;
import it.rome.game.component.SpatialComponent;

import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.managers.GroupManager;
import com.artemis.managers.TagManager;
import com.artemis.systems.VoidEntitySystem;
import com.artemis.utils.ImmutableBag;

/**
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
@Wire
public class FlappyBirdScoreSystem extends VoidEntitySystem {

  ComponentMapper<ScoreComponent> scm;
  ComponentMapper<SpatialComponent> spm;

  GroupManager groupManager;
  TagManager tagManager;

  ScoreComponent scoreComponent;

  @Override
  protected void begin() {
    super.begin();
    Entity score = tagManager.getEntity(Tags.Score);
    scoreComponent = scm.get(score);
  }

  @Override
  protected void processSystem() {
    ImmutableBag<Entity> pipes = groupManager.getEntities(Tags.Pipe);
    Entity bird = tagManager.getEntity(Tags.Bird);
    SpatialComponent birdSpatialComponent = spm.get(bird);
    for (Entity pipe : pipes) {
      SpatialComponent spatialComponent = spm.get(pipe);
      if (scm.has(pipe)) {
        if (birdSpatialComponent.getSpatial().getX() > spatialComponent.getSpatial().getX()) {
          ScoreComponent pipeScoreComponent = scm.get(pipe);
          scoreComponent.increment(pipeScoreComponent.getScore());
          pipe.edit().remove(ScoreComponent.class);
        }
      }
    }
  }

}
