package it.rome.flappy.system;

import it.rome.game.component.AccelerationComponent;
import it.rome.game.component.InputComponent;
import it.rome.game.component.SpatialComponent;
import it.rome.game.component.VelocityComponent;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.Vector2;

/**
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
@Wire
public class TapInputSystem extends EntityProcessingSystem {

  ComponentMapper<AccelerationComponent> am;
  ComponentMapper<SpatialComponent> sm;
  ComponentMapper<VelocityComponent> vm;

  @SuppressWarnings("unchecked")
  public TapInputSystem() {
    super(Aspect.getAspectForAll(InputComponent.class, VelocityComponent.class,
        AccelerationComponent.class));
  }

  @Override
  protected void process(Entity entity) {
    VelocityComponent velocityComponent = vm.get(entity);
    Vector2 velocity = velocityComponent.getSpeed();

    if (Gdx.input.justTouched() || Gdx.input.isKeyJustPressed(Keys.SPACE)) {
      velocity.y = 200;
    }

  }

}
