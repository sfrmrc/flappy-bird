package it.rome.flappy.screen;

import it.rome.flappy.Tags;
import it.rome.flappy.stage.PlayStage;
import it.rome.flappy.system.FlappyGameDirectorSystem;
import it.rome.game.screen.AbstractBaseScreen;
import it.rome.game.system.RecordingSystem;

import com.artemis.World;
import com.artemis.managers.GroupManager;
import com.artemis.managers.TagManager;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public class GameScreen extends AbstractBaseScreen {

  OrthographicCamera playCamera;

  Stage view;
  TagManager tagManager;
  GroupManager groupManager;

  private FlappyGameDirectorSystem gameDirectorSystem;

  private float w;

  private float h;

  public GameScreen(Game game) {
    super(game);
    w = Gdx.graphics.getWidth();
    h = Gdx.graphics.getHeight();
    playCamera = new OrthographicCamera(w, h);
    playCamera.setToOrtho(false, w, h);
    playCamera.update();
    initWorld();
    initView();
  }

  private void initView() {
    view = new PlayStage(world, playCamera);
  }

  private void initWorld() {
    world = new World();
    tagManager = new TagManager();
    groupManager = new GroupManager();
    gameDirectorSystem = new FlappyGameDirectorSystem(w, h, Tags.Bird);
    world.setManager(tagManager);
    world.setManager(groupManager);
    world.setSystem(gameDirectorSystem);
  }

  @Override
  public void render(float delta) {
    super.render(delta);
    world.setDelta(delta);
    world.process();
    view.act(delta);
    view.draw();
    if (gameDirectorSystem.isGameOver()) {
      game.setScreen(new MainMenuScreen(game));
    }
  }

  @Override
  public void hide() {
    super.dispose();
    view.dispose();
    world.dispose();
  }

}
