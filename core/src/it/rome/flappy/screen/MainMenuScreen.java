package it.rome.flappy.screen;

import it.rome.flappy.Fonts;
import it.rome.flappy.Images;
import it.rome.game.screen.AbstractBaseScreen;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;

/**
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public class MainMenuScreen extends AbstractBaseScreen {

  private Stage stage;
  private Label startButton;
  private Label aboutButton;

  public MainMenuScreen(final Game game) {
    super(game);
    stage = new Stage();

    LabelStyle style = new LabelStyle(Fonts.bit16, Color.WHITE);
    startButton = new Label("START", style);
    startButton.setTouchable(Touchable.enabled);
    float halfWidth = stage.getWidth() * .5f;
    float halfHeight = stage.getHeight() * .5f;
    centerActor(startButton, halfWidth, halfHeight);
    startButton.addListener(new InputListener() {
      @Override
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        game.setScreen(new GameScreen(game));
        return true;
      }
    });
    aboutButton = new Label("ABOUT", style);
    aboutButton.setTouchable(Touchable.enabled);
    centerActor(aboutButton, halfWidth, halfHeight - startButton.getHeight() - 20);
    aboutButton.addListener(new InputListener() {
      @Override
      public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        game.setScreen(new AboutScreen(game));
        return true;
      }
    });
    Image bird = new Image(Images.bird()[0]);
    centerActor(bird, halfWidth + 100, halfHeight + startButton.getHeight() + 100);
    Image splash = new Image(Images.background());
    stage.addActor(splash);
    stage.addActor(bird);
    stage.addActor(startButton);
    stage.addActor(aboutButton);
    Gdx.input.setInputProcessor(stage);
  }

  @Override
  public void render(float delta) {
    super.render(delta);
    stage.act(delta);
    stage.draw();
  }

  @Override
  public void hide() {
    stage.dispose();
  }

  private void centerActor(Actor actor, float x, float y) {
    actor.setBounds(0f, 0f, actor.getWidth(), actor.getHeight());
    actor.setPosition(x - actor.getWidth() * .5f, y - actor.getHeight() * .5f);
  }
}
