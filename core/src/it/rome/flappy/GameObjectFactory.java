package it.rome.flappy;

import it.rome.flappy.level.Level;
import it.rome.flappy.level.Pipe;
import it.rome.flappy.template.BackgroundTemplate;
import it.rome.flappy.template.BirdTemplate;
import it.rome.flappy.template.CollideTemplate;
import it.rome.flappy.template.FloorTemplate;
import it.rome.flappy.template.PipeTemplate;
import it.rome.flappy.template.PipeTemplate.Type;
import it.rome.game.attribute.Spatial;
import it.rome.game.attribute.impl.SpatialImpl;
import it.rome.game.component.ScoreComponent;
import it.rome.game.template.EntityFactory;
import it.rome.game.template.Parameters;
import it.rome.game.template.TextTemplate;

import com.artemis.Entity;
import com.artemis.World;
import com.artemis.managers.TagManager;
import com.badlogic.gdx.graphics.Color;

public class GameObjectFactory {

  static EntityFactory entityFactory;
  static World world;
  static final CollideTemplate collideTemplate = new CollideTemplate();
  static final BirdTemplate birdTemplate = new BirdTemplate();
  static final PipeTemplate pipeTemplate = new PipeTemplate();
  static final BackgroundTemplate backgroundTemplate = new BackgroundTemplate();
  static final FloorTemplate floorTemplate = new FloorTemplate();
  static final TextTemplate textTemplate = new TextTemplate();

  public static void init(World world) {
    GameObjectFactory.world = world;
    entityFactory = new EntityFactory(world);
  }

  public static void createBird(Spatial spatial) {
    entityFactory.instantiate(new Parameters().put("spatial", spatial).put("group", Tags.Collide),
        birdTemplate, collideTemplate);
  }

  public static void createPipe(float x, float height, Type type) {
    Spatial spatial = new SpatialImpl(x, height - Pipe.BASE, 30, Pipe.BASE);
    entityFactory.instantiate(
        new Parameters().put("spatial", spatial).put("type", type).put("group", Tags.Collide),
        pipeTemplate, collideTemplate);
  }

  public static void createLevel(float top, float bottom) {
    Level level = Level.Reader.load("level.txt");
    for (Pipe pipe : level.getPipes()) {
      if (Type.UP.toString().equals(pipe.getType())) {
        createPipe(pipe.getX(), top + pipe.getHeight(), Type.valueOf(pipe.getType()));
      } else {
        createPipe(pipe.getX(), bottom + pipe.getHeight(), Type.valueOf(pipe.getType()));
      }
    }
  }

  public static void createBackground(float w, float h) {
    Spatial spatial = new SpatialImpl(w * .5f, h * .5f, w, h);
    entityFactory.instantiate(new Parameters().put("spatial", spatial), backgroundTemplate);
  }

  public static void createFloor(float w, float h) {
    Spatial spatial = new SpatialImpl(w * .5f, h * .5f, w, h);
    entityFactory.instantiate(new Parameters().put("spatial", spatial).put("group", Tags.Collide),
        floorTemplate, collideTemplate);
  }

  public static void createScoreIndicator(float x, float y) {
    Spatial spatial = new SpatialImpl(x, y);
    Entity scoreIndicator =
        entityFactory.instantiate(new Parameters().put("spatial", spatial).put("font", Fonts.bit16)
            .put("color", Color.WHITE).put("text", "0").put("layer", Layers.SCORE), textTemplate);
    scoreIndicator.edit().add(new ScoreComponent(0));
    world.getManager(TagManager.class).register(Tags.Score, scoreIndicator);
  }

  public static void createText(String text, float x, float y) {
    Spatial spatial = new SpatialImpl(x, y);
    entityFactory.instantiate(new Parameters().put("spatial", spatial).put("font", Fonts.bit16)
        .put("color", Color.WHITE).put("text", text).put("layer", Layers.SCORE), textTemplate);
  }

}
