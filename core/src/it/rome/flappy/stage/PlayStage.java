package it.rome.flappy.stage;

import it.rome.flappy.GameObjectFactory;
import it.rome.flappy.Layers;
import it.rome.flappy.Tags;
import it.rome.flappy.system.FlappyBirdScoreSystem;
import it.rome.flappy.system.ScoreIndicatorSystem;
import it.rome.flappy.system.TapInputSystem;
import it.rome.game.attribute.Bound;
import it.rome.game.attribute.impl.SpatialImpl;
import it.rome.game.screen.AbstractBaseStage;
import it.rome.game.system.CollisionsSystem;
import it.rome.game.system.FpsDebugSystem;
import it.rome.game.system.RenderableSystem;
import it.rome.game.system.ScriptSystem;
import it.rome.game.system.SpriteAnimationSystem;
import it.rome.game.system.SpritePositionSystem;
import it.rome.game.system.render.Layer;
import it.rome.game.system.render.RenderableLayer;

import com.artemis.World;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public class PlayStage extends AbstractBaseStage {

  OrthographicCamera camera;
  RenderableLayer renderableLayer;
  Bound globalBound;
  float lowerBackground = 50;

  public PlayStage(World world, OrthographicCamera camera) {
    super(world);
    this.camera = camera;
    this.globalBound = new Bound(0, 0, camera.viewportWidth, camera.viewportHeight);
    GameObjectFactory.init(world);
    initRender(camera);
    initWorld();
    initPlayer();
    initLevel();
  }

  private void initLevel() {
    GameObjectFactory.createLevel(camera.viewportHeight, lowerBackground);
    GameObjectFactory.createBackground(camera.viewportWidth, camera.viewportHeight);
    GameObjectFactory.createFloor(camera.viewportWidth, lowerBackground);
  }

  private void initPlayer() {
    GameObjectFactory.createBird(new SpatialImpl(camera.viewportWidth * .5f,
        camera.viewportHeight * .5f));
    GameObjectFactory.createScoreIndicator(camera.viewportWidth * .5f, camera.viewportHeight * .5f + 100);
  }

  private void initWorld() {
    world.setSystem(new TapInputSystem());
    world.setSystem(new ScriptSystem());
    world.setSystem(new CollisionsSystem(Tags.Collide));
    world.setSystem(new FlappyBirdScoreSystem());
    world.setSystem(new ScoreIndicatorSystem());
    world.setSystem(new SpritePositionSystem());
    world.setSystem(new SpriteAnimationSystem());
    world.setSystem(new RenderableSystem(camera, renderableLayer));
    world.setSystem(new FpsDebugSystem());
    world.initialize();
  }

  private void initRender(OrthographicCamera camera) {
    SpriteBatch spriteBatch = new SpriteBatch();
    Layer background = new Layer(Layers.BACKGROUND, camera, spriteBatch);
    Layer pipes = new Layer(Layers.PIPES, camera, spriteBatch);
    Layer bird = new Layer(Layers.BIRD, camera, spriteBatch);
    Layer score = new Layer(Layers.SCORE, camera, spriteBatch);
    renderableLayer = new RenderableLayer();
    renderableLayer.add("background", background);
    renderableLayer.add("pipes", pipes);
    renderableLayer.add("play", bird);
    renderableLayer.add("score", score);
  }

};
