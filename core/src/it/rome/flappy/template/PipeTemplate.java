package it.rome.flappy.template;

import it.rome.flappy.Images;
import it.rome.flappy.Layers;
import it.rome.flappy.Tags;
import it.rome.game.attribute.Spatial;
import it.rome.game.attribute.impl.SpatialImpl;
import it.rome.game.component.AccelerationComponent;
import it.rome.game.component.RenderableComponent;
import it.rome.game.component.ScoreComponent;
import it.rome.game.component.SpatialComponent;
import it.rome.game.component.SpriteComponent;
import it.rome.game.component.VelocityComponent;
import it.rome.game.template.EntityTemplate;

import com.artemis.Entity;
import com.artemis.EntityEdit;
import com.artemis.managers.GroupManager;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public class PipeTemplate extends EntityTemplate {

  public enum Type {
    UP, DOWN
  }

  @Override
  public void apply(Entity entity) {
    EntityEdit entityEdit = entity.edit();

    Spatial initialSpatial = parameters.get("spatial");

    if (initialSpatial == null) {
      throw new IllegalArgumentException("Spatial parameter cannot be null");
    }

    Type type = parameters.get("type", Type.DOWN);

    TextureRegion texture = null;

    Spatial spatial = new SpatialImpl(initialSpatial);

    if (type == Type.DOWN) {
      texture = Images.pipeDown();
      spatial.setPosition(spatial.getX(), -Math.abs(spatial.getY()));
    } else {
      texture = Images.pipeUp();
      spatial.setPosition(spatial.getX(), Math.abs(spatial.getY()));
    }

    Sprite sprite = new Sprite(texture);

    spatial.setPosition(initialSpatial.getX(), initialSpatial.getY() + initialSpatial.getHeight()
        * .5f);

    SpatialComponent spatialComponent = new SpatialComponent(new SpatialImpl(spatial));

    entityEdit.add(new VelocityComponent(-50, 0));
    entityEdit.add(new AccelerationComponent(0, 0));
    entityEdit.add(new SpriteComponent(sprite));
    entityEdit.add(spatialComponent);
    entityEdit.add(new RenderableComponent(Layers.PIPES));
    entityEdit.add(new ScoreComponent(1));

    entity.getWorld().getManager(GroupManager.class).add(entity, Tags.Pipe);
    entity.getWorld().getManager(GroupManager.class).add(entity, Tags.Collide);

  }

}
