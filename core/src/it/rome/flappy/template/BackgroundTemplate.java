package it.rome.flappy.template;

import it.rome.flappy.Images;
import it.rome.flappy.Layers;
import it.rome.game.attribute.Spatial;
import it.rome.game.component.RenderableComponent;
import it.rome.game.component.SpatialComponent;
import it.rome.game.component.SpriteComponent;
import it.rome.game.template.EntityTemplate;

import com.artemis.Entity;
import com.artemis.EntityEdit;
import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public class BackgroundTemplate extends EntityTemplate {

  public BackgroundTemplate() {}

  @Override
  public void apply(Entity entity) {
    EntityEdit entityEdit = entity.edit();
    Spatial spatial = parameters.get("spatial");

    if (spatial == null) {
      throw new IllegalArgumentException("Spatial parameter cannot be null");
    }

    Sprite sprite = new Sprite(Images.background());

    SpriteComponent spriteComponent = new SpriteComponent(sprite);
    SpatialComponent spatialComponent = new SpatialComponent(spatial);

    entityEdit.add(spriteComponent);
    entityEdit.add(spatialComponent);
    entityEdit.add(new RenderableComponent(Layers.BACKGROUND));

  }

}
