package it.rome.flappy.template;

import it.rome.flappy.Images;
import it.rome.flappy.Layers;
import it.rome.flappy.Tags;
import it.rome.game.attribute.Script;
import it.rome.game.attribute.Spatial;
import it.rome.game.attribute.impl.ScriptJava;
import it.rome.game.attribute.impl.SpatialImpl;
import it.rome.game.component.AccelerationComponent;
import it.rome.game.component.InputComponent;
import it.rome.game.component.RenderableComponent;
import it.rome.game.component.ScriptComponent;
import it.rome.game.component.SpatialComponent;
import it.rome.game.component.SpriteAnimationComponent;
import it.rome.game.component.SpriteComponent;
import it.rome.game.component.VelocityComponent;
import it.rome.game.template.EntityTemplate;

import com.artemis.Entity;
import com.artemis.EntityEdit;
import com.artemis.World;
import com.artemis.managers.GroupManager;
import com.artemis.managers.TagManager;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

/**
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public class BirdTemplate extends EntityTemplate {

  @Override
  public void apply(Entity entity) {
    EntityEdit entityEdit = entity.edit();

    Spatial spatial = parameters.get("spatial");

    if (spatial == null) {
      throw new IllegalArgumentException("Spatial parameter cannot be null");
    }

    Sprite sprite = new Sprite(Images.bird()[0]);

    SpriteAnimationComponent spriteAnimationComponent =
        new SpriteAnimationComponent(0f, 0.2f, PlayMode.LOOP, Images.bird());

    SpatialComponent spatialComponent =
        new SpatialComponent(new SpatialImpl(spatial.getX(), spatial.getY(), sprite.getWidth(),
            sprite.getHeight()));

    Script script = new ScriptJava() {

      @Override
      public void execute(World world, Entity entity) {
        VelocityComponent velocityComponent = entity.getComponent(VelocityComponent.class);
        SpatialComponent spatialComponent = entity.getComponent(SpatialComponent.class);
        if (velocityComponent.getSpeed().y > 0) {
          spatialComponent.getSpatial().setAngle(45f);
        } else {
          spatialComponent.getSpatial().setAngle(-45f);
        }
      }

    };

    entityEdit.add(new SpriteComponent(sprite));
    entityEdit.add(spriteAnimationComponent);
    entityEdit.add(spatialComponent);
    entityEdit.add(new AccelerationComponent(0, -460));
    entityEdit.add(new VelocityComponent(new Vector2(0, 0), new Vector2(0, -200)));
    entityEdit.add(new InputComponent());
    entityEdit.add(new ScriptComponent(script));
    entityEdit.add(new RenderableComponent(Layers.BIRD));

    entity.getWorld().getManager(TagManager.class).register(Tags.Bird, entity);
    entity.getWorld().getManager(GroupManager.class).add(entity, Tags.Collide);

  }

}
