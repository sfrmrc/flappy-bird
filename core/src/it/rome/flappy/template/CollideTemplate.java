package it.rome.flappy.template;

import it.rome.game.component.CollisionComponent;
import it.rome.game.template.EntityTemplate;

import com.artemis.Entity;
import com.artemis.managers.GroupManager;

/**
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public class CollideTemplate extends EntityTemplate {

  @Override
  public void apply(Entity entity) {
    String group = parameters.get("group");

    if (group == null) {
      throw new IllegalArgumentException("Group parameter cannot be null");
    }

    entity.edit().add(new CollisionComponent());

    entity.getWorld().getManager(GroupManager.class).add(entity, group);

  }

}
