package it.rome.flappy.template;

import it.rome.flappy.Images;
import it.rome.flappy.Layers;
import it.rome.flappy.Tags;
import it.rome.game.attribute.Spatial;
import it.rome.game.attribute.impl.ScriptJava;
import it.rome.game.attribute.impl.SpatialImpl;
import it.rome.game.component.RenderableComponent;
import it.rome.game.component.ScriptComponent;
import it.rome.game.component.SpatialComponent;
import it.rome.game.component.SpriteComponent;
import it.rome.game.component.VelocityComponent;
import it.rome.game.template.EntityTemplate;

import com.artemis.Entity;
import com.artemis.EntityEdit;
import com.artemis.World;
import com.artemis.managers.GroupManager;
import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * 
 * @author m.sferra
 * 
 * @since 0.0.1
 * 
 */
public class FloorTemplate extends EntityTemplate {

  @Override
  public void apply(Entity entity) {
    EntityEdit entityEdit = entity.edit();

    Spatial initialSpatial = parameters.get("spatial");

    if (initialSpatial == null) {
      throw new IllegalArgumentException("Spatial parameter cannot be null");
    }

    Sprite sprite = new Sprite(Images.land());

    Spatial spatial = new SpatialImpl(initialSpatial);
    spatial.setSize(spatial.getWidth() * 2, spatial.getHeight());

    entityEdit.add(new SpriteComponent(sprite));
    entityEdit.add(new SpatialComponent(new SpatialImpl(spatial)));
    entityEdit.add(new RenderableComponent(Layers.SCORE));
    entityEdit.add(new VelocityComponent(-20f, 0));
    entityEdit.add(new ScriptComponent(new ScrollableScript(spatial.getX(), 0f)));

    entity.getWorld().getManager(GroupManager.class).add(entity, Tags.Collide);

  }

  private class ScrollableScript extends ScriptJava {

    float offset;
    float start;

    public ScrollableScript(float start, float offset) {
      this.start = start;
      this.offset = offset;
    }

    @Override
    public void execute(World world, Entity entity) {
      SpatialComponent spatialComponent = entity.getComponent(SpatialComponent.class);
      Spatial spatial = spatialComponent.getSpatial();
      if (spatial.getX() < offset) {
        spatial.setPosition(start, spatial.getY());
      }
    }

  }

}
