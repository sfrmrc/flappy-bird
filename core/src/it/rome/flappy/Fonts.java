package it.rome.flappy;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

public class Fonts {


  public static final BitmapFont bit16;

  static {
    bit16 = new BitmapFont(Gdx.files.internal("fonts/text.fnt"));
    bit16.setScale(.45f);
  }

  private Fonts() {}

}
