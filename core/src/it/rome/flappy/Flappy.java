package it.rome.flappy;

import it.rome.flappy.screen.MainMenuScreen;

import com.badlogic.gdx.Game;

public class Flappy extends Game {

  @Override
  public void create() {
    setScreen(new MainMenuScreen(this));
  }

}
