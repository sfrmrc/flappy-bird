package it.rome.flappy;

public class Layers {

  public static final int BACKGROUND = 0;
  public static final int PIPES = 1;
  public static final int SCORE = 2;
  public static final int BIRD = 3;

}
