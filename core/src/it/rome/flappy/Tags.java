package it.rome.flappy;

public class Tags {

  public static final String Bird = "bird";
  public static final String Collide = "collide";
  public static final String Pipe = "pipe";
  public static final String Score = "score";

}
