package it.rome.flappy;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Images {

  private static final String background = "background";
  private static final String land = "land";

  private static final String bird1 = "bird1";
  private static final String bird2 = "bird2";
  private static final String bird3 = "bird3";

  private static final String pipe1 = "pipe1";
  private static final String pipe2 = "pipe2";

  private static TextureAtlas textureAtlas = new TextureAtlas(Gdx.files.internal("flappy.txt"));

  public static TextureRegion[] bird() {
    return new TextureRegion[] {textureAtlas.findRegion(bird1), textureAtlas.findRegion(bird2),
        textureAtlas.findRegion(bird3)};
  }

  public static AtlasRegion pipeUp() {
    return textureAtlas.findRegion(pipe1);
  }

  public static AtlasRegion pipeDown() {
    return textureAtlas.findRegion(pipe2);
  }

  public static AtlasRegion background() {
    return textureAtlas.findRegion(background);
  }

  public static AtlasRegion land() {
    return textureAtlas.findRegion(land);
  }

}
