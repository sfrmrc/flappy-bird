package it.rome.flappy.desktop;

import it.rome.flappy.Flappy;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class DesktopLauncher {
  public static void main(String[] arg) {
    LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
    config.height = 480;
    config.width = 320;
    new LwjglApplication(new Flappy(), config);
  }
}
